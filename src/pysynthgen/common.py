#  Copyright (c) University of Florida 2022.
#
#  Author: Daniel Capecci, Princess Lyons
#
#  This file is a part of PySynthGen - a python port of SynthGen by Sarah Amir.
#
#  PySynthGen is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
from json import JSONEncoder

import yaml
from enum import Enum
import numpy as np
from pathlib import Path
from typing import Union
import re
import jsonpickle
import jsonpickle.ext.numpy as json_numpy

json_numpy.register_handlers()


class Sense(Enum):
    """
    Enum for min/max optimization direction
    """

    def __repr__(self):
        return self.value

    def __str__(self):
        return f'{self.name}'

    minimize = 1
    maximize = -1


class Benchmarks(Enum):
    iscas = 'iscas'
    epfl = 'epfl'
    mcnc = 'mcnc'

    @property
    def names(self) -> [str]:
        if self == Benchmarks.iscas:
            return ["c1355", "c17", "c1908", "c2670", "c3540", "c432", "c499",
                    "c5315", "c6288", "c7552", "c880"]
        elif self == Benchmarks.epfl:
            return ["arbiter", "bar", "cavlc", "ctrl", "dec", "div", "hyp", "i2c",
                    "int2float", "log2", "max", "multiplier", "priority", "router",
                    "sin", "sqrt", "square", "voter"]
        elif self == Benchmarks.mcnc:
            return [
                "ex7", "al2", "example2", "o64", "alcom", "opa", "alu1", "f5xp1",
                "p82", "alu2", "frg1", "pair", "alu4", "frg2", "parity", "amd",
                "pcler8", "apex1", "i1", "pcle", "apex2", "i2", "pm1", "apex3",
                "i3", "pope", "apex4", "i4", "prom1", "apex5", "i5", "prom2",
                "apex6", "i6", "rd53", "apex7", "i7", "rd73", "i8", "rd84", "i9",
                "risc", "ibm", "root", "in0", "rot", "bc0", "in1", "ryy6", "br1",
                "in2", "s9sym", "br2", "in3", "sao2", "c1355", "in4", "sct", "c17",
                "in5", "seq", "c1908", "in6", "sex", "c2670", "in7", "shift", "c3540",
                "intb", "signet", "c432", "jbp", "soar", "c499", "k2", "sqn", "c5315",
                "lal", "sqr6", "c6288", "lin", "sqrt8", "c7552", "luc", "squar5",
                "c880", "m1", "t1", "c8", "m2", "t3", "cc", "m3", "t481", "chkn",
                "m4", "table3", "cht", "mainpla", "table5", "clip", "majority",
                "tcon", "max1024", "term1", "cm138a", "max128", "ti", "cm150a",
                "max46", "tms", "cm151a", "max512", "too_large", "cm152a", "misex1",
                "ts10", "cm162a", "misex2", "ttt2", "cm163a", "misex3", "cm42a", "misg",
                "unreg", "cm82a", "mish", "vda", "cm85a", "misj", "vg2", "cmb", "mlp4",
                "vtx1", "comp", "mp2d", "x1dn", "con1", "mux", "x1", "cordic", "my_adder",
                "x2dn", "count", "newapla1", "x2", "cps", "newapla2", "x3", "cu", "newapla",
                "x4", "dalu", "newbyte", "x6dn", "dc1", "newcond", "x7dn", "dc2", "newcpla1",
                "x9dn", "decod", "newcpla2", "xor5", "des", "newcwp", "xparc", "dist", "newill",
                "duke2", "newtag", "Z5xp1", "e64", "newtpla1", "Z9sym", "ex4", "newtpla2", "ex5",
                "newtpla"
            ]

    @staticmethod
    def namesDict():
        return {b.value: b.names for b in Benchmarks}

    @staticmethod
    def flatNames() -> [str]:
        names = []
        for b in Benchmarks:
            names.extend(b.names)
        return names


def _getArray(string):
    return np.array([int(s) for s in re.split(r'\s+', string) if len(s) > 0])


def _getArrayFromDict(_dict, key):
    try:
        return [int(s) for s in re.split(r'\s+', _dict[key].strip('() '))]
    except KeyError:
        return None


def _get2dArray(strings: [str]):
    nums = []
    for string in strings:
        nums.append([int(s) for s in re.split(r'\s+', string.strip())])
    return np.array(nums)


class ReferenceStats:
    """
    Helper class encapsulating the loading/caching of reference benchmark info
    """

    @staticmethod
    def fromPath(filePath: Path, ignoreCachedJson=False):
        suffix = '.refstats'
        if not ignoreCachedJson and filePath.with_suffix(suffix).exists():
            try:
                rf = ReferenceStats(**jsonpickle.decode(filePath.with_suffix(suffix).read_text()))
                if isinstance(rf, ReferenceStats):
                    return rf
            except:
                pass
        fileText = filePath.read_text()
        lines = [line for line in fileText.splitlines() if ':' in line]
        fileText = "\n".join(lines)
        valueDict = yaml.load(fileText, yaml.Loader)
        stats = ReferenceStats(
            numGates=valueDict.get('Number_of_Nodes', None),
            numConnections=valueDict.get('Number_of_Edges', None),
            maxDelay=valueDict.get('Maximum_Delay', None),
            nodes=_getArrayFromDict(valueDict, 'Node_shape'),
            inputs=_getArrayFromDict(valueDict, 'Input_shape'),
            outputs=_getArrayFromDict(valueDict, 'Output_shape'),
            latched=_getArrayFromDict(valueDict, 'Latched_shape'),
            POs=_getArrayFromDict(valueDict, 'POshape'),
            edges=_getArrayFromDict(valueDict, 'Edge_length_distribution'),
            fanouts=_getArrayFromDict(valueDict, 'Fanout_distribution')
        )
        filePath.with_suffix(suffix).write_text(jsonpickle.encode(stats, unpicklable=False))
        return stats

    def __init__(
        self,
        numGates,
        numConnections,
        maxDelay,
        nodes,
        inputs,
        outputs,
        latched,
        POs,
        edges,
        fanouts,
        **kwargs
    ):
        self.numGates = numGates
        self.numConnections = numConnections
        self.maxDelay = maxDelay
        self.nodes = nodes
        self.inputs = inputs
        self.outputs = outputs
        self.latched = latched
        self.POs = POs
        self.edges = edges
        self.fanouts = fanouts


class SolutionStatsEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, SolutionStats):
            return obj.__dict__
        elif isinstance(obj, Sense):
            obj.__str__()
        return JSONEncoder.default(self, obj)


def _tryExcept(func, *args, **kwargs):
    try:
        return func(*args, **kwargs)
    except:
        return None


class SolutionStats:
    """
    Encapsulates solution information for generating netlist
    """

    @staticmethod
    def fromPath(filePath: Path, ignoreCachedJson=False):
        import re
        suffix = '.solstats'
        if not ignoreCachedJson and filePath.with_suffix(suffix).exists():
            try:
                return SolutionStats(**jsonpickle.decode(filePath.with_suffix(suffix).read_text()))
            except:
                pass
        fileText = filePath.read_text()
        if filePath.suffix == '.txt':
            stats = SolutionStats(
                nodes=_getArray(re.search(r"Node\s+([-\d+\s]+)\n", fileText).group(1)),
                inputs=_getArray(re.search(r"Input\s+([-\d+\s]+)\n", fileText).group(1)),
                outputs=_getArray(re.search(r"Output\s+([-\d+\s]+)\n", fileText).group(1)),
                latched=_getArray(re.search(r"Latched\s+([-\d+\s]+)\n", fileText).group(1)),
                POs=_getArray(re.search(r"PO\s+([-\d+\s]+)\n", fileText).group(1)),
                edges=_getArray(re.search(r"Edge\s+([-\d+\s]+)\n+", fileText).group(1)),
                fanouts=_getArray(re.search(r"Fanout\s+([-\d+\s]+)\n+", fileText).group(1)),
                edgeTable=_get2dArray(re.search('EdgeTable\n([-\d+\s\n]+)', fileText).group(1).strip().splitlines()),
                fanoutTable=_get2dArray(
                    re.search('FanoutTable\n([-\d+\s\n]+)', fileText).group(1).strip().splitlines()),
                faninTable=_get2dArray(re.search('FaninTable\n([-\d+\s\n]+)', fileText).group(1).strip().splitlines()),
            )
        else:
            stats = SolutionStats(**json.loads(filePath.read_text()))
        stats.save(filePath)
        return stats

    def __init__(
        self,
        nodes,
        inputs,
        outputs,
        latched,
        POs,
        edges,
        fanouts,
        edgeTable,
        fanoutTable,
        faninTable,
        meta,
    ):
        self.nodes = _tryExcept(lambda x: np.array(x).astype(int), nodes)
        self.inputs = _tryExcept(lambda x: np.array(x).astype(int), inputs)
        self.outputs = _tryExcept(lambda x: np.array(x).astype(int), outputs)
        self.latched = _tryExcept(lambda x: np.array(x).astype(int), latched)
        self.POs = _tryExcept(lambda x: np.array(x).astype(int), POs)
        self.edges = _tryExcept(lambda x: np.array(x).astype(int), edges)
        self.fanouts = _tryExcept(lambda x: np.array(x).astype(int), fanouts)
        self.edgeTable = _tryExcept(lambda x: np.array(x).astype(int), edgeTable)
        self.fanoutTable = _tryExcept(lambda x: np.array(x).astype(int), fanoutTable)
        self.faninTable = _tryExcept(lambda x: np.array(x).astype(int), faninTable)
        self.meta = meta

    def save(self, path: Path, suffix='.solstats'):
        path.parent.mkdir(parents=True, exist_ok=True)
        path.with_suffix(suffix).write_text(json.dumps(self, cls=SolutionStatsEncoder))


def padStats(stats: Union[ReferenceStats, SolutionStats], depth: int):
    latchedNew = None
    numNodes = len(stats.nodes)
    if depth == numNodes:
        nodeNew = stats.nodes
        poNew = stats.POs
        edgeNew = stats.edges
        inputNew = stats.inputs
        outputNew = stats.outputs
    elif depth < numNodes:
        nodeNew = stats.nodes[:depth]
        poNew = stats.POs[:depth]
        edgeNew = stats.edges[:depth]
        inputNew = stats.inputs[:depth]
        outputNew = np.concatenate((stats.outputs[:depth - 1], np.array([0])))
    else:  # depth > numNodes
        b = int(np.ceil(depth / numNodes))
        a = int(b - 1)
        n1 = int(b * numNodes - depth)
        n2 = int(depth - a * numNodes)
        nodeNew = np.array([])
        inputNew = np.array([])
        outputNew = np.array([])
        poNew = np.array([])
        for i in range(n1):
            nnew = stats.nodes[i] * np.ones((a,))
            nodeNew = np.concatenate((nodeNew, nnew))

            innew = np.append(stats.inputs[i], stats.nodes[i] * np.ones((a - 1,)))
            inputNew = np.concatenate((inputNew, innew))

            onew = np.append(stats.nodes[i] * np.ones((a - 1,)), stats.outputs[i])
            outputNew = np.concatenate((outputNew, onew))

            pnew = np.append(np.zeros((a - 1,)), stats.POs[i])
            poNew = np.concatenate((poNew, pnew))
        for i in range(n2):
            nnew = stats.nodes[n1 + i] * np.ones((b,))
            nodeNew = np.concatenate((nodeNew, nnew))

            innew = np.append(stats.inputs[n1 + i], stats.nodes[n1 + i] * np.ones((b - 1,)))
            inputNew = np.concatenate((inputNew, innew))

            onew = np.append(stats.nodes[n1 + i] * np.ones((b - 1,)), stats.outputs[n1 + i])
            outputNew = np.concatenate((outputNew, onew))

            pnew = np.append(np.zeros((b - 1,)), stats.POs[n1 + i])
            poNew = np.concatenate((poNew, pnew))

        edgeNew = stats.edges
        latchedNew = stats.latched
        if depth > len(edgeNew):
            edgeNew = np.append(edgeNew, [0] * (depth - len(edgeNew)))
            latchedNew = np.append(latchedNew, [0] * (depth - len(latchedNew)))
        # TODO: double check this '1 indexing'
        edgeNew[1] = stats.edges[1] + np.sum(nodeNew) - np.sum(stats.nodes)
        poNew[depth - 1] = 1

    # Fanout
    if len(stats.fanouts) > depth:
        fanoutNew = stats.fanouts[:depth]
    elif len(stats.fanouts) < depth:
        fanoutNew = np.concatenate((stats.fanouts, np.zeros((depth - len(stats.fanouts),))))
    else:
        fanoutNew = stats.fanouts
    if np.sum(nodeNew) > np.sum(stats.nodes):
        fanoutNew[1] = stats.fanouts[1] + np.sum(nodeNew) - np.sum(stats.nodes)

    return nodeNew, poNew, inputNew, outputNew, edgeNew, fanoutNew, latchedNew
