#  Copyright (c) University of Florida 2022.
#
#  Author: Daniel Capecci, Princess Lyons
#
#  This file is a part of PySynthGen - a python port of SynthGen by Sarah Amir.
#
#  PySynthGen is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

# %%
from __future__ import annotations

import random
from pathlib import Path
import argparse
import re

from .common import Sense, SolutionStats
from .netlist import NetlistGenerator, NetlistType
from .synth import Synthesizer


class GateWeight(argparse.Action):
    """
    Mapping for gate weighting arguments
    """

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())
        for value in values:
            k, v = re.split('[=:]', value)
            getattr(namespace, self.dest)[k.upper()] = int(v)


def RangeType(x):
    return [int(s) for s in re.split('[-\s]+', x)]


def getArgs():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-sd', '--saveDir',
        default='./output',
        type=lambda x: Path(x).expanduser().resolve(),
        help='Directory to save synthesized designs'
    )

    parser.add_argument(
        '-bn', '--benchName',
        default='bench',
        help='set a name for the benchmark'
    )

    parser.add_argument(
        '-bi', '--benchId',
        default=0,
        type=int,
        help='Identifier for configuration (if numBench is greater than 1, ids start with this)'
    )

    parser.add_argument(
        '-nb', '--numBench',
        default=1,
        type=int,
        help='Number of benchmarks to create with given parameters'
    )

    parser.add_argument(
        '-n', '--numNodes',
        default=5000,
        type=int,
        help='Number of gates in benchmark',
    )

    parser.add_argument(
        '-nr', '--numNodesRange',
        type=RangeType,
        default=None,
        help='Randomly selects integers within this range for number of nodes in a generated benchmark. '
             'Overrides --numNodes.'
    )

    parser.add_argument(
        '-d', '--depth',
        default=30,
        type=int,
        help='Depth of the bench to synthesize'
    )

    parser.add_argument(
        '-in', '--inputs',
        default=200,
        type=int,
        help='Specify number of inputs for benchmark(s)'
    )

    parser.add_argument(
        '-inr', '--inputsRange',
        type=RangeType,
        default=None,
        help='Randomly selects integers within this range for number of inputs in a generated benchmark. '
             'Overrides --inputs.'
    )
    parser.add_argument(
        '-out', '--outputs',
        default=100,
        type=int,
        help='Specify number of outputs for benchmark(s)'
    )
    parser.add_argument(
        '-outr', '--outputsRange',
        default=None,
        type=RangeType,
        help='Randomly selects integers within this range for number of outputs in a generated benchmark. '
             'Overrides --outputs.'
    )

    parser.add_argument(
        '-t', '--timeout',
        type=int,
        default=7200,
        help='Timeout for solution.'
    )

    parser.add_argument(
        '-mfi', '--maxFanin',
        type=int,
        default=3,
        help='Max fan in of nodes'
    )

    parser.add_argument(
        '-mfo', '--maxFanout',
        type=int,
        default=3,
        help='Max fan out of nodes'
    )

    parser.add_argument(
        '-s', '--sense',
        default='min',
        choices=['min', 'max'],
        help='Optimization direction.'
    )

    parser.add_argument(
        '-ot', '--outType',
        default='gsc',
        type=NetlistType.fromString,
        choices=[nt.value for nt in NetlistType],
        help='Output file types. See NetlistType for more info.'
    )

    parser.add_argument(
        '-ips', '--ignorePrevSol',
        action='store_true',
        default=False,
        help='By default, solutions look for previous solutions of same name and include '
             'in constraints to ensure varying solutions for same params.  '
             'Can add this flag to ignore previous solutions.'
    )

    parser.add_argument(
        '-f', '--force',
        default=False,
        action='store_true',
        help='Force Synthesizer to solve LP even if solved file already exists (overwrites existing solution)'
    )

    parser.add_argument(
        '--debug',
        default=False,
        action='store_true',
        help='Debugging utility to compare solutions to MATLAB solutions.  Forces all default parameters.'
    )

    parser.add_argument(
        '--debugMatPath',
        default=None,
        type=lambda p: Path(p).expanduser().resolve(),
        help='Path to debug MAT file to test matrices and constraints against. Created by saving entire MATLAB environment after running SynthGen. '
             'By default, uses included tests/debug.mat file.'
    )

    parser.add_argument(
        '-gw', '--gateWeights',
        default=None,
        nargs='*',
        action=GateWeight,
        help='Specify integer weights for likelihood of gate insertion',
    )

    args, _ = parser.parse_known_args()

    if args.debug:  # enforce defaults for debugging against MATLAB output
        for k, v in vars(args).items():
            if k not in ['force']:
                args.__setattr__(k, parser.get_default(k))
    return args


def main(args=None):
    print(" © Copyright 2021 University of Florida Research Foundation, Inc. All Rights Reserved.")
    print(" This program comes with ABSOLUTELY NO WARRANTY;")
    print(" This is free software, and you are welcome to redistribute it under certain conditions;")
    print()

    args = getArgs() if args is None else args

    debug = args.debug

    outDir = Path(args.saveDir).expanduser().resolve()

    print(f'Saving output to directory: {outDir}')

    try:
        for bId in range(args.benchId, args.benchId + args.numBench):
            synth = Synthesizer(
                outDir=outDir,
                minMax=Sense.minimize if args.sense == 'min' else Sense.maximize,
                maxFanIn=args.maxFanin,
                maxFanOut=args.maxFanout,
                debug=debug,
                debugMatPath=args.debugMatPath
            )

            numPrimaryInputs = args.inputs if args.inputsRange is None else random.randint(*args.inputsRange)
            numPrimaryOutputs = args.outputs if args.outputsRange is None else random.randint(*args.outputsRange)
            numNodes = args.numNodes if args.numNodesRange is None else random.randint(*args.numNodesRange)
            depth = args.depth

            dirName = f'{args.benchName}_n{numNodes}d{depth}i{numPrimaryInputs}o{numPrimaryOutputs}'
            filename = f'{dirName}_{bId}'
            savePath = outDir / dirName / f'{filename}.solstats'

            if savePath.exists() and not args.force:
                print(
                    '*********************************************************\n'
                    f'Solution exists: {savePath.as_posix()}\n\n'
                    f'Set FORCE (-f) flag to force new solution and overwrite existing solution.\n\n'
                    f'Returning results from {savePath.name}\n'
                    '*********************************************************'
                )
                solStats = SolutionStats.fromPath(savePath, args.force)
            else:
                solStats = synth.solve(
                    name=dirName,
                    idx=bId,
                    numNodes=numNodes,
                    depth=args.depth,
                    numPrimaryInputs=numPrimaryInputs,
                    numPrimaryOutputs=numPrimaryOutputs,
                    timeout=args.timeout,
                    solver='mip',
                    includeSynthStats=not args.ignorePrevSol,
                )

                solStats.save(savePath)

            if solStats is not None:
                gweights = {}
                if args.gateWeights is not None:
                    gweights.update(args.gateWeights)
                    print(f'Using gate weights: {gweights}')
                gener = NetlistGenerator(
                    inputFileName=savePath,
                    outType=args.outType,
                    **gweights
                )
                gener.run(solStats)
            else:
                print("!!! No Solution Found !!!")
    except KeyboardInterrupt:
        print('Exiting....')


if __name__ == '__main__':
    main()
