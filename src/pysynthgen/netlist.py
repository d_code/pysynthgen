#  Copyright (c) University of Florida 2022.
#
#  Author: Daniel Capecci, Princess Lyons
#
#  This file is a part of PySynthGen - a python port of SynthGen by Sarah Amir.
#
#  PySynthGen is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import json
import random
import datetime
import re
from collections import OrderedDict
from enum import Enum
from pathlib import Path
from typing import Dict, List

from .common import SolutionStats


class NetlistType(Enum):
    """
    verilog: library independent verilog
    gsc: uses gate name from GSC library
    bench: outputs a bench file (e.g. for use in Berkely ABC tool)
    all: will output all file types
    """
    verilog = 'verilog'
    gsc = 'gsc'
    bench = 'bench'
    all = 'all'

    @staticmethod
    def fromString(string):
        for t in NetlistType:
            if re.search(string.lower(), t.value) is not None:
                return t


def gateType(gateName):
    g = re.search(r'([A-z]+)(?:\d+X\d+)?', gateName).group(1)
    return g


class NetlistGenerator:

    def __init__(
        self,
        inputFileName: Path,
        outType: NetlistType = NetlistType.gsc,
        **gateWeights
    ):
        self.modName: str = ""
        self.verbose: bool = True
        self.errorFlag: bool = False
        self.name: str = ""
        self.parentPath: str = ""
        self.inputFileName = inputFileName
        self.outType = outType

        self.gateWeights = OrderedDict(
            AND=1,
            OR=1,
            XOR=1,
            XNOR=1,
            NAND=1,
            NOR=1,
            NOT=1,
            BUF=1,
            INV=1,
        )

        if gateWeights is not None:
            self.gateWeights.update(gateWeights)

        self.modName = inputFileName.stem
        if "/" in self.modName:
            self.modName = self.modName.split("/", 2)[1]

        self._inputStats = None
        self._verilogLines: List[str] = []
        self._benchLines: List[str] = []

    def run(self, stats: SolutionStats):
        self.__genCkt(stats)
        self.save(self.inputFileName)

    def _weightsForGateTypes(self, gTypes):
        return [self.gateWeights[gateType(k)] for k in gTypes]

    def __genCkt(self, stats: SolutionStats):
        """

        :param stats:
        :type stats:
        :return:
        :rtype:
        """
        depth = len(stats.nodes)  # Org: node.size()
        inputBank: Dict[int, List[str]] = {i: [] for i in range(depth)}  # dict()
        outputBank: Dict[int, List[str]] = {i: [] for i in range(depth)}  # dict()
        wirecount = 0
        for dep in range(depth):
            outs: List[str] = []
            for fo in range(depth):
                for j in range(stats.fanoutTable[dep][fo]):
                    wirecount += 1
                    wirename: str = f"n{wirecount}"
                    outputBank[dep].append(wirename)
                    outs.extend([wirename for _ in range(fo)])
            temp: int = 0
            for length in range(depth):
                for k in range(stats.edgeTable[dep][length]):
                    wirename = outs[temp]
                    temp = temp + 1
                    if dep + length <= depth:
                        inputBank[dep + length].append(wirename)

        POs: List[str] = []
        for i in range(depth):
            for j in range(stats.POs[i]):
                rand = random.randint(0, len(outputBank[i]) - 1)
                while outputBank[i][rand] in POs:
                    rand = random.randint(0, stats.nodes[i] - 1)
                POs.append(outputBank[i][rand])

        gatecount: int = 0
        for d in range(1, depth):
            # loop through fanouts - no , loop through node
            # randomly pick a output , remove output from bank
            # randomly pick inputs , remove input from bank
            # make gates - Logic\LUT
            fanins: List[int] = []
            for i in range(depth):
                nVals = stats.faninTable[d][i]
                fanins.extend([i + 1] * nVals)
            for n in range(stats.nodes[d]):
                temp = random.randint(0, len(outputBank[d]) - 1)
                out = outputBank[d][temp]  # Remove temp
                outputBank[d].remove(out)  # NOTE: may need to use .pop for these
                ins = []
                for i in range(fanins[n]):
                    if len(inputBank[d]) == 0:
                        continue
                    temp = random.randint(0, len(inputBank[d]) - 1)
                    ins.append(inputBank[d][temp])
                    inputBank[d].remove(inputBank[d][temp])

                ins = set(ins)  # Remove duplicates
                lenIns = len(ins)
                if self.outType in [NetlistType.verilog, NetlistType.all]:
                    Logic1 = ["NOT", "BUF"]
                    Logic2 = ["AND", "OR", "XOR", "XNOR", "NAND", "NOR"]
                    if lenIns == 1:
                        logic = random.choices(Logic1, self._weightsForGateTypes(Logic1), k=1)[0]
                    else:
                        logic = random.choices(Logic2, self._weightsForGateTypes(Logic2), k=1)[0]
                    line = f'{logic} g{gatecount} ({out}, '
                    for i, _v in enumerate(ins):
                        line += _v
                        if i == lenIns - 1:
                            line += ");"
                        else:
                            line += ", "
                    self._verilogLines.append(line)
                if self.outType in [NetlistType.bench, NetlistType.all]:
                    Logic1 = ["NOT", "BUF"]
                    Logic2 = ["AND", "OR", "XOR", "XNOR", "NAND", "NOR"]
                    if lenIns == 1:
                        logic = random.choices(Logic1, self._weightsForGateTypes(Logic1), k=1)[0]
                    else:
                        logic = random.choices(Logic2, self._weightsForGateTypes(Logic2), k=1)[0]
                    line = f"{out} = {logic} ("
                    for i, _v in enumerate(ins):
                        line += f"{_v}"
                        if i == lenIns - 1:
                            line += ")"
                        else:
                            line += ", "
                    self._benchLines.append(line)

                if self.outType == NetlistType.gsc:
                    logic = ""
                    Logic4 = ["NAND", "NOR", "OR"]  # 4X1
                    Logic3 = ["NAND", "NOR"]  # 3X1
                    Logic2 = ["NAND", "NAND2X2", "AND", "NOR", "XOR", "OR"]  # 2X1
                    Logic1 = ["INV", "BUF"]  # X1
                    if lenIns == 1:
                        logic = random.choices(Logic1, self._weightsForGateTypes(Logic1), k=1)[0]
                    elif lenIns == 2:
                        logic = random.choices(Logic2, self._weightsForGateTypes(Logic2), k=1)[0]
                    elif lenIns == 3:
                        logic = random.choices(Logic3, self._weightsForGateTypes(Logic3), k=1)[0]
                    elif lenIns == 4:
                        logic = random.choices(Logic4, self._weightsForGateTypes(Logic4), k=1)[0]
                    else:
                        print(f"Error in number of inputs: {lenIns} not between 1 and 4")
                    if re.search(r'\dX\d', logic) is None:
                        if lenIns == 1:
                            logic += 'X1'
                        else:
                            logic += f'{lenIns}X1'
                    line = f"{logic} g{gatecount} ("
                    pinNames = ["A", "B", "C", "D"]
                    for i, _v in enumerate(ins):
                        line += f".{pinNames[i]}({_v}), "
                    line += f".Y({out}));"
                    self._verilogLines.append(line)

                gatecount += 1

        now = datetime.datetime.now().astimezone().isoformat()  # strftime("%Y/%m/%d %H:%M:%S")

        if self.outType in [NetlistType.verilog, NetlistType.all, NetlistType.gsc]:  # ['v', 'verilog', 'both', 'GSC']:
            temp = 0
            self._verilogLines.insert(temp, f"// Benchmark {self.modName} written by PySynthGen on {now}")
            temp += 1

            line = f"module {self.modName} ("
            for i, _v in enumerate(outputBank[0]):  # inputs
                line = f'{line} {_v},'
                if i == len(outputBank[0]) - 1:
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                elif (i + 1) % 8 == 0:
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
            for i, _v in enumerate(POs):  # outputs
                line = f"{line} {_v}"
                if i == len(POs) - 1:
                    line = line + ");"
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                elif (i + 1) % 8 == 0:
                    line = line + ","
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                else:
                    line = line + ","
            self._verilogLines.insert(temp, line)

            temp += 1
            line = "input"  # OutputBank.get(0)
            for i, _v in enumerate(outputBank[0]):
                line = f'{line} {_v}'
                if i == len(outputBank[0]) - 1:
                    line = line + ";"
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                elif (i + 1) % 8 == 0:
                    line = line + ","
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                else:
                    line = line + ","
            self._verilogLines.insert(temp, line)

            temp += 1
            line = "output"  # POs
            for i, _v in enumerate(POs):
                line = f'{line} {_v}'
                if i == (len(POs) - 1):
                    line += ";"
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                elif (i + 1) % 8 == 0:
                    line += ","
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                else:
                    line += ","
            self._verilogLines.insert(temp, line)

            temp += 1
            line = "wire"  # n + wirecount
            wires = []
            for i in range(len(outputBank[0]) + 1, wirecount):
                if f'n{i}' not in POs:
                    wires.append(f'n{i}')

            for i, _w in enumerate(wires):
                line = f'{line} {wires[i]}'
                if i == len(wires) - 1:
                    line += ";"
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                elif (i + 1) % 8 == 0:
                    line += ","
                    self._verilogLines.insert(temp, line)
                    temp += 1
                    line = ""
                else:
                    line += ","
            self._verilogLines.insert(temp, line)
            temp += 1
            self._verilogLines.append("endmodule")

        if self.outType in [NetlistType.bench, NetlistType.all]:
            temp = 0
            self._benchLines.insert(temp, f"# Benchmark {self.modName} written by PySynthGen on {now}\n\n")
            temp += 1
            for i, _v in enumerate(outputBank[0]):
                self._benchLines.insert(temp, f"INPUT({_v})")  # OutputBank.get(0)
                temp += 1
            for i, _v in enumerate(POs):
                self._benchLines.insert(temp, f"OUTPUT({_v})")  # POs
                temp += 1

    def save(self, inputFileName: Path):
        writePaths = []

        if self.outType in [NetlistType.verilog, NetlistType.gsc, NetlistType.all]:
            outPath = inputFileName.with_suffix(".v")
            writePaths.append((outPath, self._verilogLines))

        if self.outType in [NetlistType.bench, NetlistType.all]:
            outPath = inputFileName.with_suffix(".bench")
            writePaths.append((outPath, self._benchLines))

        if len(writePaths) == 0:
            raise Exception(f'Unsupported output type: {self.outType}')

        for p, lines in writePaths:
            p.write_text("\n".join(lines))
            print(f"Saved: {p}")

        metaPath = inputFileName.with_suffix('.json')
        metaPath.write_text(json.dumps(dict(
            gateWeight=self.gateWeights,
        )))
