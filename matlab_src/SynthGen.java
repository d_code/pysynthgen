// ęCopyright 2020 University of Florida Research Foundation, Inc. All Rights Reserved.
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation version 3
//
// This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
//import Graphical.CircuitVisualizer;
import java.time.format.DateTimeFormatter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;  

public class SynthGen {
	ArrayList<String> fileVerilog = new ArrayList<String>();
	ArrayList<String> fileBench = new ArrayList<String>();
	public HashMap<String, ArrayList<Integer>> inputData = new HashMap<String, ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>> EdgeTable = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>> FanoutTable = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>> FaninTable = new ArrayList<ArrayList<Integer>>();
	String modName = "";
	boolean verbose = true;
//	boolean viz = true;
	public boolean errorFlag = false;
	String name = "";
	String parentPath = "";
	int maxFanin = 3;
	
	public SynthGen(){}
	
	public SynthGen(String inputFileName, String outType){
		modName = inputFileName.replace(".txt", "");
		if (modName.contains("/")) {
			modName = modName.split("/",2)[1];
		}
		ReadStats(inputFileName);
		GenCkt(modName, outType);
		WriteFile(inputFileName, outType);
	}

	private void GenCkt(String modName, String outType) {
		ArrayList<Integer> node  = inputData.get("Node"); 
//		ArrayList<Integer> input  = inputData.get("Input"); 
//		ArrayList<Integer> output  = inputData.get("Output"); 
		ArrayList<Integer> po  = inputData.get("PO"); 
//		ArrayList<Integer> edge  = inputData.get("Edge"); 
//		ArrayList<Integer> fanout  = inputData.get("FO");
		int depth = node.size();
		
		HashMap<Integer,ArrayList<String>> InputBank = new HashMap<Integer,ArrayList<String>>();
		HashMap<Integer,ArrayList<String>> OutputBank = new HashMap<Integer,ArrayList<String>>();
		for (int i = 0; i < depth; i++) {
			ArrayList<String> temp = new ArrayList<String>();
			InputBank.put(i,temp);
		}
		for (int i = 0; i < depth; i++) {
			ArrayList<String> temp = new ArrayList<String>();
			OutputBank.put(i,temp);
		}
		int wirecount = 0;
		for (int dep = 0; dep < depth; dep++) {
			ArrayList<String> outs = new ArrayList<String>();
			for (int fo = 0; fo < depth; fo++) {
				for (int j = 0; j < FanoutTable.get(dep).get(fo); j++) {
					wirecount++;
					String wirename = "n"+wirecount;
					OutputBank.get(dep).add(wirename);
					for (int k = 0; k < fo; k++) {
						outs.add(wirename);
					}					
				}
			}
			int temp = 0;
			for (int len = 0; len < depth; len++) {
				for (int k = 0; k < EdgeTable.get(dep).get(len); k++) {
					String wirename = outs.get(temp);
					temp++;
					if (dep+len <= depth) {
						InputBank.get(dep+len).add(wirename);
					
					}
				}
			}		
		}
		
		Random rn = new Random();
		ArrayList<String> POs = new ArrayList<String>();
		for (int i = 0; i < depth; i++) {
			for (int j = 0; j < po.get(i); j++) {
				int rand = rn.nextInt(OutputBank.get(i).size());
				while (POs.contains(OutputBank.get(i).get(rand))) {
					rand = rn.nextInt(node.get(i));
				} 
				POs.add(OutputBank.get(i).get(rand));
			}
		}
//		System.out.println(POs);
		
		int gatecount = 0;
		for (int d = 1; d < depth; d++) { // foreach depth
			/*
		 	loop through fanouts - no , loop through node
		 	randomly pick a output , remove output from bank
			randomly pick inputs , remove input from bank
			make gates - Logic\LUT
			 */
			ArrayList<Integer>fanins = new ArrayList<Integer>();
			for (int i = 0 ; i < depth; i++) {
				for (int j = 0; j < FaninTable.get(d).get(i); j++) {
					fanins.add(i+1);
				}
			}
			for (int n = 0; n < node.get(d); n++) { // foreach gate in depth d
				int temp = rn.nextInt(OutputBank.get(d).size());
				String out = OutputBank.get(d).get(temp); 
				OutputBank.get(d).remove(temp);
				ArrayList<String>ins = new ArrayList<String>();
				for (int i = 0; i < fanins.get(n); i++) {
					temp = rn.nextInt(InputBank.get(d).size());
					ins.add(InputBank.get(d).get(temp)); 
					InputBank.get(d).remove(temp);					
				}
				String line = "";
				if(outType.equals("v")|outType.equals("verilog")|outType.equals("duo")){
					for (int i = 0; i < ins.size(); i++) {
						String oneip = ins.get(i);
						while(ins.contains(oneip)){
							ins.remove(oneip);
						}
						ins.add(oneip);
					}
					String logic = "";	
					String[] Logic2 = {"and ","or  ","xor ","xnor","nand","nor "};
					String[] Logic1 = {"not ","buf "};
					if(ins.size()==1){ 
						logic = Logic1[rn.nextInt(Logic1.length)];
					}else{
						logic = Logic2[rn.nextInt(Logic2.length)];
					}
					line = logic + " g" + gatecount + " (" + out + ", ";
					for (int i = 0; i < ins.size(); i++) {
						line += ins.get(i);
						if(i==ins.size()-1){
							line += ");";
						}else{
							line += ", ";
						}
					}	
					fileVerilog.add(line);
				}
				if(outType.equals("bench")|outType.equals("duo")){
					String logic = "";	
					String[] Logic2 = {"AND ","OR  ","XOR ","XNOR","NAND","NOR "};
					String[] Logic1 = {"NOT ","BUF "};
					if(ins.size()==1){ 
						logic = Logic1[rn.nextInt(Logic1.length)];
					}else{
						logic = Logic2[rn.nextInt(Logic2.length)];
					}
					line = out + " = " + logic + " (";
					for (int i = 0; i < ins.size(); i++) {
						line += ins.get(i);
						if(i==ins.size()-1){
							line += ")";
						}else{
							line += ", ";
						}
					}
					fileBench.add(line);
				}
				if(outType.equals("GSC")){
					for (int i = 0; i < ins.size(); i++) {
						String oneip = ins.get(i);
						while(ins.contains(oneip)){
							ins.remove(oneip);
						}
						ins.add(oneip);
					}
					String logic = "";
					String[] Logic4 = {"NAND4X1","NOR4X1 ","OR4X1  "};
					String[] Logic3 = {"NAND3X1","NOR3X1 "};
					String[] Logic2 = {"NAND2X1","NAND2X2","AND2X1 ","NOR2X1 ","XOR2X1 ","OR2X1  "}; // "XNOR2X1",
					String[] Logic1 = {"INVX1  ","BUFX1  "};
					if(ins.size()==1){ 
						logic = Logic1[rn.nextInt(Logic1.length)];
					}else if(ins.size()==2){ 
						logic = Logic2[rn.nextInt(Logic2.length)];
					}else if(ins.size()==3){ 
						logic = Logic3[rn.nextInt(Logic3.length)];
					}else if(ins.size()==4){ 
						logic = Logic4[rn.nextInt(Logic4.length)];
					} else {
						System.err.println("Error in number of input. (SynthGen:188)");
					}
					line = logic + " g" + gatecount + " (" ; //TODO
					String[] pinNames = {".A",".B",".C",".D"};
					for (int i = 0; i < ins.size(); i++) {
						line += pinNames[i]+"("+ins.get(i)+"), ";
					}	
					line += ".Y(" + out +"));" ;
					fileVerilog.add(line);
				}
				
				gatecount++;
			}
		}
		

		
		
	   /*
	    *  Finishing part
	    *  Defining terms
	    */
		
		
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	   LocalDateTime now = LocalDateTime.now(); 
	   System.out.println(dtf.format(now));  
		   
		if(outType.equals("v")|outType.equals("verilog")|outType.equals("duo")|outType.equals("GSC")){
			int temp = 0;
			fileVerilog.add(temp,"// Benchmark "+modName+" written by SynthGen on " + dtf.format(now));
			temp++;
			
			String line = "module "+modName+" ("; 
			for (int i = 0; i < OutputBank.get(0).size(); i++) { // inputs
				line = line + " " + OutputBank.get(0).get(i) + ",";
				if (i == OutputBank.get(0).size()-1){
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else if ((i+1)%8 == 0){
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				}
			}
			for (int i = 0; i < POs.size(); i++) { // outputs
				line = line + " " + POs.get(i);
				if (i == POs.size()-1){
					line = line + ");";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else if ((i+1)%8 == 0){
					line = line + ",";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else {
					line = line + ",";
				}
			}
			fileVerilog.add(temp,line);
			temp++;
			line = "";
			
			line = "input"; //OutputBank.get(0)
			for (int i = 0; i < OutputBank.get(0).size(); i++) {
				line = line + " " + OutputBank.get(0).get(i);
				if (i == OutputBank.get(0).size()-1){
					line = line + ";";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else if ((i+1)%8 == 0){
					line = line + ",";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else {
					line = line + ",";
				}
			}
			fileVerilog.add(temp,line);
			temp++;
			line = "";
			
			line = "output"; //POs
			for (int i = 0; i < POs.size(); i++) {
				line = line + " " + POs.get(i);
				if (i == POs.size()-1){
					line = line + ";";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else if ((i+1)%8 == 0){
					line = line + ",";
					fileVerilog.add(temp,line);
					temp++;
					line = "";
				} else {
					line = line + ",";
				}
			}
			fileVerilog.add(temp,line);
			temp++;
			line = "";
			
			line = "wire"; // n + wirecount
			ArrayList<String> wires = new ArrayList<String>();
			for (int i = OutputBank.get(0).size()+1; i <= wirecount; i++) {
				if (! POs.contains("n"+i)) {
					wires.add("n"+i);
				}
			}
			for (int i = 0; i < wires.size(); i++) {
				line = line + " " + wires.get(i);
				if (i == wires.size()-1) {
					line = line + ";";
					fileVerilog.add(temp, line);
					temp++;
					line = "";
				} else if ((i + 1) % 8 == 0) {
					line = line + ",";
					fileVerilog.add(temp, line);
					temp++;
					line = "";
				} else {
					line = line + ",";
				} 
			}
			fileVerilog.add(temp,line);
			temp++;
			line = "";			
			
			fileVerilog.add("endmodule");
			
		}
		if(outType.equals("bench")|outType.equals("duo")){
			int temp = 0;
			fileBench.add(temp,"# Benchmark "+modName+" written by SynthGen on " + dtf.format(now));
			temp++;
			
			for (int i = 0; i < OutputBank.get(0).size(); i++) {
				fileBench.add(temp,"INPUT("+ OutputBank.get(0).get(i) +")"); //OutputBank.get(0)
				temp++;
			}
			
			for (int i = 0; i < POs.size(); i++) {
				fileBench.add(temp,"OUTPUT("+ POs.get(i) +")"); //POs
				temp++;
			}			
		}
	}
		
	public void ReadStats(String inputFileName) {
		ArrayList<String> statsFile = fileReader(inputFileName);
		ArrayList<Integer> nodeShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> inputShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> outputShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> latchShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> poShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> edgeShape  = new ArrayList<Integer>(); 
		ArrayList<Integer> foShape  = new ArrayList<Integer>();

		for (int i = 0; i < statsFile.size(); i++) {
			String line = statsFile.get(i);
//			System.out.println(line);
			if(line.length()!=0){
				String[] words = line.split(" ",2)[1].trim().split(" ");
				switch (line.split(" ",2)[0]) {
				case "Node":
					for (int j = 0; j < words.length; j++) {
						nodeShape .add(Integer.parseInt(words[j]));
					}
					break;
				case "Input":
					for (int j = 0; j < words.length; j++) {
						inputShape.add(Integer.parseInt(words[j]));
					}	
					break;
				case "Output":
					for (int j = 0; j < words.length; j++) {
						outputShape.add(Integer.parseInt(words[j]));
					}
					break;
				case "Latched":
					for (int j = 0; j < words.length; j++) {
						latchShape.add(Integer.parseInt(words[j]));
					}
					break;
				case "PO":
					for (int j = 0; j < words.length; j++) {
						poShape.add(Integer.parseInt(words[j]));
					}
					break;
				case "Edge":
					for (int j = 0; j < words.length; j++) {
						edgeShape.add(Integer.parseInt(words[j]));
					}
					break;
				case "Fanout":
					for (int j = 0; j < words.length; j++) {
						foShape.add(Integer.parseInt(words[j]));
					}
					break;
				case "EdgeTable":
					for (int j = 0; j < nodeShape.size(); j++) {
						ArrayList<Integer> ETj = new ArrayList<Integer>();
						i++;
						words = statsFile.get(i).trim().split(" ");
						for (int k = 0; k < words.length; k++) {
							ETj.add(Integer.parseInt(words[k]));
						}
						EdgeTable.add(ETj);
					}				
					break;	
				case "FanoutTable":
					for (int j = 0; j < nodeShape.size(); j++) {
						ArrayList<Integer> FTj = new ArrayList<Integer>();
						i++;
						words = statsFile.get(i).trim().split(" ");
						for (int k = 0; k < words.length; k++) {
							FTj.add(Integer.parseInt(words[k]));
						}
						FanoutTable.add(FTj);
					}
					break;	
				case "FaninTable":
					for (int j = 0; j < nodeShape.size(); j++) {
						ArrayList<Integer> FTj = new ArrayList<Integer>();
						i++;
						words = statsFile.get(i).trim().split(" ");
						for (int k = 0; k < words.length; k++) {
							FTj.add(Integer.parseInt(words[k]));
						}
						FaninTable.add(FTj);
					}
					break;
				default:
					break;
				}
			}	
		}
		inputData.put("Node", nodeShape);
		inputData.put("Input", inputShape);
		inputData.put("Output", outputShape);
		inputData.put("Latch", latchShape);
		inputData.put("PO",  poShape);
		inputData.put("Edge", edgeShape);
		inputData.put("FO", foShape);
	}
	
	private ArrayList<String> fileReader (String inputFileName) {
		ArrayList<String> lines = new ArrayList<String>();
		File file = new File(inputFileName);
		name = file.getName();
		parentPath = file.getParent();
		if( ! file.exists() ) {
			System.err.println("File "+ inputFileName +" does not exist.");
			errorFlag = true;
		}
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(file));
			String newline = "";
			while( ( newline = buffer.readLine() ) != null ){
				lines.add(newline);
			}
			buffer.close();	
		} catch (FileNotFoundException e) {
			System.err.println("File "+ inputFileName +" does not exist.");
			errorFlag = true;
		} catch (IOException e) {
			System.err.println("IOException error occured in " + inputFileName);
			errorFlag = true;
			e.printStackTrace();
		}
		return lines;
	}
	
	private void WriteFile(String inputFileName, String outType) {
		if (outType.equals("v")|outType.equals("verilog")|outType.equals("GSC")) {
			fileWriter(fileVerilog, inputFileName.replace(".txt", ".v"));
			System.out.println("File written: "+ inputFileName.replace(".txt",".v"));
		}else if(outType.equals("bench")){
			fileWriter(fileBench, inputFileName.replace(".txt", ".bench"));
			System.out.println("File written: "+ inputFileName.replace(".txt",".bench"));
		} else if(outType.equals("duo"))	{
			fileWriter(fileVerilog, inputFileName.replace(".txt", ".v"));
			System.out.println("File written: "+ inputFileName.replace(".txt",".v"));
			fileWriter(fileBench, inputFileName.replace(".txt", ".bench"));
			System.out.println("File written: "+ inputFileName.replace(".txt",".bench"));
		}
		
		
//		if(outType.equals("v")){
//			if (viz) {
//				CircuitVisualizer cktviz = new CircuitVisualizer(inputFileName.replace(".txt","."+outType));
//			}
//		}
	}
	
	private void fileWriter (ArrayList<String> inputList, String outputFileName){
  		File file = new File(outputFileName);
  		try {
			PrintWriter writer = new PrintWriter(file);
			for (String line : inputList) {
				writer.println(line);
			}			
			writer.close();
		} catch (FileNotFoundException e) {
			errorFlag = true;
			e.printStackTrace();
		}
  	}

	
	public static void main(String[] args) {

		System.out.println(" ęCopyright 2020 University of Florida Research Foundation, Inc. All Rights Reserved.");
		System.out.println(" This program comes with ABSOLUTELY NO WARRANTY;");
		System.out.println(" This is free software, and you are welcome to redistribute it under certain conditions;");
		System.out.println("");
		if (args.length>1) {
			SynthGen sg = new SynthGen(args[0], args[1]);
		} else {
			System.out.println("Usage: java SynthGen <fileName> <outputType>");
		}
		
	}
}
