          
fileID = fopen(char(address),'r');

Node_shape = fgetl(fileID);
Node_shape = Node_shape(7:length(Node_shape));
if(Node_shape(1)==' ')
    Node_shape = Node_shape(2:length(Node_shape));
end
nodeShape = strsplit(Node_shape);
for i = 1 : length(nodeShape)
   Node(i) =  str2num(char(nodeShape(i)));
end
clear nodeShape; clear Node_shape;


Input_shape = fgetl(fileID);
Input_shape = Input_shape(8:length(Input_shape));
if(Input_shape(1)==' ')
    Input_shape = Input_shape(2:length(Input_shape));
end
inputShape = strsplit(Input_shape);
for i = 1 : length(inputShape)
   Input(i) =  str2num(char(inputShape(i)));
end
clear inputShape; clear Input_shape;



Output_shape = fgetl(fileID);
Output_shape = Output_shape(9:length(Output_shape));
if(Output_shape(1)==' ')
    Output_shape = Output_shape(2:length(Output_shape));
end
outputShape = strsplit(Output_shape);
for i = 1 : length(outputShape)
   Output(i) =  str2num(char(outputShape(i)));
end
clear outputShape; clear Output_shape;



Latched_shape = fgetl(fileID);
Latched_shape = Latched_shape(10:length(Latched_shape));
if(Latched_shape(1)==' ')
    Latched_shape = Latched_shape(2:length(Latched_shape));
end
latchedShape = strsplit(Latched_shape);
for i = 1 : length(latchedShape)
   Latched(i) =  str2num(char(latchedShape(i)));
end
clear latchedShape; clear Latched_shape;



POshape = fgetl(fileID);
POshape = POshape(5:length(POshape));
if(POshape(1)==' ')
    POshape = POshape(2:length(POshape));
end
poShape = strsplit(POshape);
for i = 1 : length(poShape)
   PO(i) =  str2num(char(poShape(i)));
end
clear poShape; clear POshape;


Edge_length_distribution = fgetl(fileID);
Edge_length_distribution = Edge_length_distribution(7:length(Edge_length_distribution));
if(Edge_length_distribution(1)==' ')
    Edge_length_distribution = Edge_length_distribution(2:length(Edge_length_distribution));
end
edgeLength = strsplit(Edge_length_distribution);
for i = 1 : length(edgeLength)
   Edge(i) =  str2num(char(edgeLength(i)));
end
clear edgeLength; clear Edge_length_distribution;


Fanout_distribution = fgetl(fileID);
Fanout_distribution = Fanout_distribution(9:length(Fanout_distribution));
if(Fanout_distribution(1)==' ')
    Fanout_distribution = Fanout_distribution(2:length(Fanout_distribution));
end
fanoutShape = strsplit(Fanout_distribution);
for i = 1 : length(fanoutShape)
   Fanout(i) =  str2num(char(fanoutShape(i)));
end
clear fanoutShape; clear Fanout_distribution;

fclose(fileID);
clear address; clear i; clear fileID, clear tline;
