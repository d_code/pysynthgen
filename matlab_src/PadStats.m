%% Equality
if (depth==length(Node))
        Nodeall(kk,:) = Node;
        POall(kk,:) = PO;
%         Fanoutall(kk,:) = Fanout;
        Edgeall(kk,:) = Edge;
        Inputall(kk,:) = Input;
        Outputall(kk,:) = Output;
%         Latchedall(kk,:) = Latched;    


else   
% Inequality      
        nn=length(Node);
%% Desired depth is smaller
    if (depth < nn)
        Nodeall(kk,:) = Node(1:depth);
        Inputall(kk,:) = Input(1:depth);
        Outputall(kk,:) = [Output(1:depth-1) 0];
        POall(kk,:) = PO(1:depth);
        Edgeall(kk,:) = Edge(1:depth);
%         Fanoutall(kk,:) = Fanout(1:depth);
%         Latchedall(kk,:) = Latched(1:depth); 

%% Desired depth is larger
    elseif (depth > nn)
%         a = floor(depth/nn);
        b = ceil(depth/nn);
        a = b - 1;
        n1 = b * nn - depth;
        n2 = depth - a * nn;       
        % first n1 number of element will be copied a times
        % last n2 number of element will be copied b times
        % n1+n2 = nn; a*n1 + b*n2 = depth;
        for i = 1:n1
            NodeNew   (        a*(i-1)+1 :            a*i ) = Node(i)*ones(1,a);
            InputNew  (        a*(i-1)+1 :            a*i ) = [Input(i) Node(i)*ones(1,a-1);];
            OutputNew (        a*(i-1)+1 :            a*i ) = [Node(i)*ones(1,a-1) Output(i)];
            PONew     (        a*(i-1)+1 :            a*i ) = [zeros(1,a-1) PO(i)];
        end
        for i = 1:n2
            NodeNew   ( a*n1 + b*(i-1)+1 : a*n1 + b*i ) = Node(n1 +i)*ones(1,b);
            InputNew  ( a*n1 + b*(i-1)+1 : a*n1 + b*i ) = [Input(n1+i) Node(n1 +i)*ones(1,b-1)];
            OutputNew ( a*n1 + b*(i-1)+1 : a*n1 + b*i ) = [Node(n1 +i)*ones(1,b-1) Output(n1+i)];   
            PONew     ( a*n1 + b*(i-1)+1 : a*n1 + b*i ) = [zeros(1,b-1) PO(n1+i)];
        end
           
%         %% Edge Table for Reference
%         A1 = zeros(1,nn^2);
%         for i=1:nn
%             A1(nn*(i-1)+i) = 1;
%             A1(nn*i-i+2 : nn*i) = ones(1,i-1);
%         end 
%     %     A1(nn*(nn-1)+1:nn^2) = 1;
%         B1 = 0;
%         A2 = zeros(nn,nn^2);    
%         for i = 1:nn
%             A2(i,nn*(i-1)+1:nn*i)=ones(1,nn);
%         end
%         B2 = transpose(Output);
%         A3 = zeros(1,nn);
%         for j=1:nn
%             for i = 1:nn
%                 A3(j,nn*(i-1)+j) = 1;
%             end
%         end
%         B3 = transpose(Edge);
%         A=[A2
%             A3
%             A1];
%         B=[B2
%             B3
%             B1];
%         sol = intlinprog([],ones(1,nn^2),[],[],A,B,zeros(1,nn^2),[],[]);
%         clear ET0;
%         for i = 1:nn
%             ET0(i,1:nn) = sol(nn*(i-1)+1:nn*i);
%         end
%         clear ET;        
%         ET = zeros(depth,depth);
%         for i = 1:n1
%             for j = 1:a-1
%                 ET(a*(i-1) + j,2) = Node(i);
%             end
%             ET(a*i,2) = ET0(i,2);
%             for j = 3:nn-i+1
%                 len0 = j-1;
%                 dest0 = i + len0;
%                 if(dest0<=n1)
%                     dest = a*(dest0-1) + 1;
%                 else
%                     dest = a*n1 + b*(dest0-n1-1) + 1;
%                 end
%                 len = dest - a*i;
%                 ET(a*i, len) = ET0(i,j);
%             end
%         end
%         for i = 1:n2
%             for j = 1:b-1
%                 ET(n1*a + (i-1)*b + j,2) = Node(n1+i);
%             end
%             ET(n1*a + i*b,2) = ET0(n1+i,2);
%             for j = 3:nn-i-a*n1+1
%                 len0 = j-1;
%                 dest0 = n1 + i + len0;
%                 dest = a*n1 + b*(dest0-n1-1) + 1;
%                 len = dest - b*i -a*n1;
%                 ET(a*n1 + b*i, len) = ET0(n1+i,j);
%             end
%         end
%         for i=1:depth
%             EdgeNew(i) = sum(ET(:,i));
%         end
%         

%         PONew=PO; 
 
        %% Assignment
        
        Nodeall(kk,:) = NodeNew;
        Inputall(kk,:) = InputNew;
        Outputall(kk,:) = OutputNew;
%         Fanoutall(kk,:) = FanoutNew;
%         Latchedall(kk,:) = LatchedNew;    
        EdgeNew=Edge; LatchedNew=Latched;
        for i = length(Edge)+1 : depth
%                 PONew(i-1)=0; %FanoutNew(i)=1;
                EdgeNew(i)=0; LatchedNew(i)=0;
        end
        EdgeNew(2) = Edge(2) + sum(Nodeall(kk,:)) - sum(Node);
        PONew(depth)=1;  
        Edgeall(kk,:) = EdgeNew;
        POall(kk,:) = PONew;
    end
end

%% Fanout
if (length(Fanout) > depth)
    FanoutNew = Fanout(1:depth);
elseif (length(Fanout) < depth)
    FanoutNew = [Fanout zeros(1,depth-length(Fanout))];
else % equal
    FanoutNew = Fanout;
end
if (sum(Nodeall(kk,:)) > sum(Node))
    FanoutNew(2) = Fanout(2) + sum(Nodeall(kk,:)) - sum(Node);
end
Fanoutall(kk,:) = FanoutNew;

kk = kk + 1;
%% Clearing
clear Node Input Output PO Edge Fanout Latched;
clear NodeNew InputNew OutputNew PONew EdgeNew FanoutNew LatchedNew;
clear i nn MaximumDelay NoOfConnection NoOfGates;% ET0;
clear a b A1 B1 A2 B2 A3 B3 A B n1 n2;