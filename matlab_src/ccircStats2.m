clear Node Input Output PO Edge Fanout;

fileID = fopen(char(address),'r');
for i = 1 : 2
    tline = fgetl(fileID);
end
NoOfGates = fgetl(fileID); NoOfGates = str2double(NoOfGates(19:length(NoOfGates)));
NoOfConnection = fgetl(fileID); NoOfConnection = str2double(NoOfConnection(18:length(NoOfConnection)));
MaximumDelay = fgetl(fileID); MaximumDelay = str2double(MaximumDelay(16:length(MaximumDelay)));
for i = 6 : 31
    tline = fgetl(fileID);
end

Node_shape = fgetl(fileID);
Node_shape = Node_shape(15:length(Node_shape)-2);
nodeShape = strsplit(Node_shape);
for i = 1 : length(nodeShape)
   Node(i) =  str2num(char(nodeShape(i)));
end
clear nodeShape; clear Node_shape;


Input_shape = fgetl(fileID);
Input_shape = Input_shape(16:length(Input_shape)-2);
inputShape = strsplit(Input_shape);
for i = 1 : length(inputShape)
   Input(i) =  str2num(char(inputShape(i)));
end
clear inputShape; clear Input_shape;



Output_shape = fgetl(fileID);
Output_shape = Output_shape(17:length(Output_shape)-2);
outputShape = strsplit(Output_shape);
for i = 1 : length(outputShape)
   Output(i) =  str2num(char(outputShape(i)));
end
clear outputShape; clear Output_shape;



Latched_shape = fgetl(fileID);
Latched_shape = Latched_shape(18:length(Latched_shape)-2);
latchedShape = strsplit(Latched_shape);
for i = 1 : length(latchedShape)
   Latched(i) =  str2num(char(latchedShape(i)));
end
clear latchedShape; clear Latched_shape;



POshape = fgetl(fileID);
POshape = POshape(12:length(POshape)-2);
poShape = strsplit(POshape);
for i = 1 : length(poShape)
   PO(i) =  str2num(char(poShape(i)));
end
clear poShape; clear POshape;


Edge_length_distribution = fgetl(fileID);
Edge_length_distribution = Edge_length_distribution(29:length(Edge_length_distribution)-2);
edgeLength = strsplit(Edge_length_distribution);
for i = 1 : length(edgeLength)
   Edge(i) =  str2num(char(edgeLength(i)));
end
clear edgeLength; clear Edge_length_distribution;


Fanout_distribution = fgetl(fileID);
Fanout_distribution = Fanout_distribution(24:length(Fanout_distribution)-2);
fanoutShape = strsplit(Fanout_distribution);
for i = 1 : length(fanoutShape)
   Fanout(i) =  str2num(char(fanoutShape(i)));
end
clear fanoutShape; clear Fanout_distribution;


fclose(fileID);
clear address; clear i; clear fileID, clear tline;
